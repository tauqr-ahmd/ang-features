import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutComponent } from './page-layout/page-layout.component';
import { HomeComponent } from './home/home.component';
import { PageRoutingModule } from './page-routing.module';

@NgModule({
  declarations: [PageLayoutComponent, HomeComponent],
  imports: [CommonModule, PageRoutingModule],
})
export class PagesModule {}
