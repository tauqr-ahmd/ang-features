import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FeatureListComponent } from './feature-list/feature-list.component';
import { ExportAsComponent } from './export-as/export-as.component';
import { FeaturesLayoutComponent } from './features-layout/features-layout.component';

const featureRoutes: Routes = [
  {
    path: '',
    component: FeaturesLayoutComponent,
    children: [
      {
        path: '',
        component: FeatureListComponent,
        data: { title: 'All Features' },
      },
      {
        path: 'export-as',
        component: ExportAsComponent,
        data: { title: 'Export As' },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(featureRoutes)],
  exports: [RouterModule],
})
export class FeaturesRoutingModule {}
