import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExportAsModule } from 'ngx-export-as';

import { ExportAsComponent } from './export-as/export-as.component';
import { FeatureListComponent } from './feature-list/feature-list.component';
import { FeaturesRoutingModule } from './features-routing.module';
import { FeaturesLayoutComponent } from './features-layout/features-layout.component';

@NgModule({
  declarations: [ExportAsComponent, FeatureListComponent, FeaturesLayoutComponent],
  imports: [CommonModule, ExportAsModule, FeaturesRoutingModule],
})
export class FeaturesModule {}
