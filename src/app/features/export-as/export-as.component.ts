import { Component, OnInit } from '@angular/core';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';

@Component({
  selector: 'export-as',
  templateUrl: './export-as.component.html',
  styleUrls: ['./export-as.component.scss'],
})
export class ExportAsComponent implements OnInit {
  exportToExcel: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementIdOrContent: 'orderList',
  };
  exportToPDF: ExportAsConfig = {
    type: 'pdf', // the type you want to download
    elementIdOrContent: 'orderList',
  };
  exportToCSV: ExportAsConfig = {
    type: 'csv', // the type you want to download
    elementIdOrContent: 'orderList',
  };
  public orderList: any[] = [
    {
      order_id: 2,
      user_name: 'Charles W. Abeyta',
      sub_time: '00:00:00',
      name: 'new_man',
      qty: 2,
      status: 'Completed',
      total_price: 100,
      last_notification_time: null,
      completion_time: null,
    },
    {
      order_id: 2,
      user_name: 'Charles W. Abeyta',
      sub_time: '00:00:00',
      name: 'Pizza al Pesto',
      qty: 1,
      status: 'Completed',
      total_price: 90,
      last_notification_time: null,
      completion_time: null,
    },
    {
      order_id: 3,
      user_name: 'Charles W. Abeyta',
      sub_time: '00:00:00',
      name: 'chola',
      qty: 1,
      status: 'Completed',
      total_price: 400,
      last_notification_time: null,
      completion_time: 72775,
    },
    {
      order_id: 4,
      user_name: 'Charles W. Abeyta',
      sub_time: '00:00:00',
      name: 'Pizza al Pesto',
      qty: 1,
      status: 'New',
      total_price: 30,
      last_notification_time: null,
      completion_time: null,
    },
    {
      order_id: 5,
      user_name: 'Charles W. Abeyta',
      sub_time: '00:00:00',
      name: 'chola',
      qty: 1,
      status: 'New',
      total_price: 30,
      last_notification_time: null,
      completion_time: null,
    },
    {
      order_id: 6,
      user_name: 'Charles W. Abeyta',
      sub_time: '11:00:00',
      name: 'chola',
      qty: 3,
      status: 'New',
      total_price: 180,
      last_notification_time: null,
      completion_time: null,
    },
  ];
  constructor(private exportAs: ExportAsService) {}

  ngOnInit() {
    console.log('Order list: ', this.orderList);
    const tableHeadings = this.orderList.map((order) => {
      return Object.keys(order);
    })[0];
    // set this as table headings
    // use ngFor
    console.log('table headings: ', tableHeadings);
    const tableValues = this.orderList.map((order) => {
      return Object.values(order);
    });
    // set this as table entries
    // use ngFor
    console.log('table entries: ', tableValues);
  }

  exportAsExcel() {
    // download the file using old school javascript method
    this.exportAs.save(this.exportToExcel, 'orders').subscribe(() => {
      // save started
      console.log('exported to Excel file');
    });
  }

  exportAsPDF() {
    // download the file using old school javascript method
    this.exportAs.save(this.exportToPDF, 'orders').subscribe(() => {
      // save started
      console.log('exported to PDF file');
    });
  }

  exportAsCSV() {
    // download the file using old school javascript method
    this.exportAs.save(this.exportToCSV, 'orders').subscribe(() => {
      // save started
      console.log('exported to CSV file');
    });
  }
}
